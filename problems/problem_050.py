# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(lst):
    first_half = []
    sec_half = []
    length = len(lst)
    for index in range(length):
        if index < ((length) / 2):
            first_half.append(lst[index])
        else:
            sec_half.append(lst[index])
    return first_half, sec_half


print(halve_the_list([1, 3, 5, 7, 5]))
