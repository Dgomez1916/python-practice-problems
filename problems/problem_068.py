
class Person:
    def __init__(self, name, hated_foods, loved_foods):
        self.name = name
        self.hated_foods = hated_foods
        self.loved_foods = loved_foods

    def taste(self, food):
        if food in self.hated_foods:
            return False
        elif food in self.loved_foods:
            return True
        else:
            return None


person = Person("Danny", ["peas", "bologna"], ["wine", "tacos"])

print(person.taste("lasagna"))
print(person.taste("peas"))
print(person.taste("wine"))
