# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    new_max = max(values)
    return new_max


print(max_in_list([50, 70, 80]))
