# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def find_second_largest(values):
#     second_large_lst = set(values)
#     second_large_lst.remove(max(values))
#     return (max(second_large_lst))


# print(find_second_largest([12, 3, 44, 55]))


def find_second_largest(values):

    if len(values) <= 1:
        return None
    second_highest = values.copy()

    second_highest.remove(max(values))
    return max(second_highest)


print(find_second_largest([12, 3, 440, 55]))


# if len(values) <= 1:
#     return None
# max_value = [0]
# second_value = None
# for value in values[1:]:
#     if value > max_value:
#         second_value = max_value
#         max_value = value
#     elif second_value is None or value > second_value:
#         second_value = value
# return second_value
