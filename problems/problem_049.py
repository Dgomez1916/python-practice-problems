# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#

def sum_two_numbers(x, y):
    return (x + y)


print(sum_two_numbers(200, 400))
