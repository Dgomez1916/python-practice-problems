
class Book:
    def __init__(self, author, title):
        self.author = author
        self.title = title

    def get_author(self):
        return "Author:" + self.author

    def get_title(self):
        return "Title:" + self.title


book = Book("Natalie Zina Walschots", "Hench")
print(book.get_author())
print(book.get_title())
