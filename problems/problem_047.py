# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lowercase = False
    uppercase = False
    min_digit = False
    special_char = False

    for char in password:
        if char.isalpha():
            if char.isupper():
                uppercase = True
            else:
                if char.islower():
                    lowercase = True
        elif char.isdigit():
            min_digit = True
        elif char == "$" or char == "!" or char == "@" or char == "*":
            special_char = True
    return (
        len(password) >= 6 and
        len(password) <= 12 and
        lowercase and
        uppercase and
        min_digit and
        special_char
    )


print(check_password('Gumbo123*'))
