# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random


def generate_lotto_numbers():
    lotto_nums = []
    while len(lotto_nums) < 6:
        lotto_num = random.randint(1, 40)
        if lotto_num not in lotto_nums:
            lotto_nums.append(lotto_num)

    return lotto_nums


print(generate_lotto_numbers())

